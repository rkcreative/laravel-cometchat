<?php

namespace RKCreative\LaravelCometChat;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Message;
use function json_decode;
use function mb_strtolower;

class CometChat
{
    private static $apiKey;
    private static $apiUrl;
    private static $appId;

    /**
     * CometChat Api constructor.
     */
    public function __construct()
    {
        self::$apiKey = config('cometchat.api_key');
        self::$apiUrl = config('cometchat.api_url');
        self::$appId = mb_strtolower(config('cometchat.app_id'));
    }

    /**
     * Create a user
     *
     * @param $uid
     * @param  null  $name
     * @param  array  $parameters
     *
     * @return false|\Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function createUser(
        $uid,
        $name,
        $parameters = []
    ) {
        $path = '/users';
        $method = 'POST';
        $parameters['uid'] = $uid;
        $parameters['name'] = $name;

        return self::sendRequest($path, $method, $parameters);
    }

    /**
     * Update a user
     *
     * @param $uid
     * @param  null  $parameters
     *
     * @return false|\Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function updateUser(
        $uid,
        $parameters = []
    ) {
        $path = '/users/'.$uid;
        $method = 'PUT';

        return self::sendRequest($path, $method, $parameters);
    }

    /**
     * Delete a user
     *
     * @param $uid
     *
     * @return \Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function deleteUser($uid)
    {
        $path = '/users/'.$uid;
        $method = 'DELETE';
        $parameters['permanent'] = true;

        return self::sendRequest($path, $method, $parameters);
    }

    /**
     * Deactivate a user
     *
     * @param $uid
     *
     * @return \Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function deactivateUser($uid)
    {
        $path = '/users/'.$uid;
        $method = 'DELETE';
        $parameters['permanent'] = false;

        return self::sendRequest($path, $method, $parameters);
    }

    /**
     * Reactivate a user or users
     *
     * @param $uids
     *
     * @return \Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function reactivateUser($uids)
    {
        $path = '/users';
        $method = 'PUT';
        $parameters['uidsToActivate'] = $uids;

        return self::sendRequest($path, $method, $parameters);
    }

    /**
     * Create a user auth token
     *
     * @param $uid
     * @param  bool  $force
     *
     * @return \Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function createToken($uid, $force = false)
    {
        $path = '/users/'.$uid.'/auth_tokens';
        $method = 'POST';
        $parameters['force'] = $force;

        return self::sendRequest($path, $method, $parameters);
    }

    /**
     * Delete a user auth token
     *
     * @param $uid
     * @param $authToken
     *
     * @return \Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function deleteToken($uid, $authToken)
    {
        $path = '/users/'.$uid.'/auth_tokens/'.$authToken;
        $method = 'DELETE';

        return self::sendRequest($path, $method);
    }

    /**
     * Block Users
     *
     * @param $uid
     * @param $blockedUsers
     *
     * @return \Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function blockUsers($uid, $blockedUsers = [])
    {
        $path = '/users/'.$uid.'/blockedusers';
        $method = 'POST';
        $parameters['blockedUids'] = $blockedUsers;

        return self::sendRequest($path, $method, $parameters);
    }

    /**
     * Unblock Users
     *
     * @param $uid
     * @param $blockedUsers
     *
     * @return \Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function unblockUsers($uid, $blockedUsers = [])
    {
        $path = '/users/'.$uid.'/blockedusers';
        $method = 'DELETE';
        $parameters['blockedUids'] = $blockedUsers;

        return self::sendRequest($path, $method, $parameters);
    }

    /**
     * Add Friends
     *
     * @param $uid
     * @param $friends
     *
     * @return \Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function addFriends($uid, $friends = [])
    {
        $path = '/users/'.$uid.'/friends';
        $method = 'POST';
        $parameters['accepted'] = $friends;

        return self::sendRequest($path, $method, $parameters);
    }

    /**
     * Delete Friends
     *
     * @param $uid
     * @param $friends
     *
     * @return \Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function deleteFriends($uid, $friends = [])
    {
        $path = '/users/'.$uid.'/friends';
        $method = 'DELETE';
        $parameters['friends'] = $friends;

        return self::sendRequest($path, $method, $parameters);
    }

    /**
     * Send a request and retrieve the response from the CometChat API
     *
     * @param $path
     * @param $method
     * @param $data
     *
     * @return false|\Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private static function sendRequest($path, $method, $data = null)
    {
        try {
            $client = new Client();


            $response = $client->request($method, self::$apiUrl.$path, [
                'headers' => [
                    'Accept'       => 'application/json',
                    'Content-Type' => 'application/json',
                    'apiKey'       => self::$apiKey,
                    'appId'        => self::$appId,
                ],
                'json'    => $data,
            ]);

            if ($response->getStatusCode() == '200') {
                $data = (string) $response->getBody();

                return json_decode($data);
            }

            return false;
        } catch (RequestException $e) {
            echo Message::toString($e->getRequest());
            if ($e->hasResponse()) {
                echo Message::toString($e->getResponse());
            }
        }
    }
}
