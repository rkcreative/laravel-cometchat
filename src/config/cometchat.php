<?php

return [
    /*
    |--------------------------------------------------------------------------
    | CometChat API Key
    |--------------------------------------------------------------------------
    |
    | You can generate a rest api key for your app from the CometChat dashboard
    | and enter it here
    |
    */
    'api_key' => env('COMETCHAT_API_KEY', ''),

    /*
    |--------------------------------------------------------------------------
    | CometChat API Url
    |--------------------------------------------------------------------------
    |
    | Add the CometChat API url here
    |
    */
    'api_url' => env('COMETCHAT_API_URL', 'https://api-us.cometchat.io/v2.0'),

    /*
    |--------------------------------------------------------------------------
    | App ID
    |--------------------------------------------------------------------------
    |
    | The App ID that was generated when you created your app from the dashboard.
    |
    */
    'app_id'  => config('COMETCHAT_APP_ID', ''),
];
