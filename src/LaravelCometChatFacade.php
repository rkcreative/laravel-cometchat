<?php

namespace RKCreative\LaravelCometChat;

use Illuminate\Support\Facades\Facade;

/**
 * Class LaravelMesiboApiFacade
 *
 * @package RKCreative\LaravelMesiboApi
 */
class LaravelCometChatFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'cometchat';
    }
}
