# Laravel CometChat API

[![Latest Stable Version](https://poser.pugx.org/rkcreative/laravel-cometchat/v)](//packagist.org/packages/rkcreative/laravel-cometchat)
[![Total Downloads](https://poser.pugx.org/rkcreative/laravel-cometchat/downloads)](//packagist.org/packages/rkcreative/laravel-cometchat)
[![Latest Unstable Version](https://poser.pugx.org/rkcreative/laravel-cometchat/v/unstable)](//packagist.org/packages/rkcreative/laravel-cometchat)
[![License](https://poser.pugx.org/rkcreative/laravel-cometchat/license)](//packagist.org/packages/rkcreative/laravel-cometchat)

Laravel package for integrating the [CometChat API](https://prodocs.cometchat.com/reference) into your app.

## Dependencies

- [PHP](https://secure.php.net): ^7.2
- [guzzlehttp/guzzle](https://github.com/guzzle/guzzle): 5.\* | 6.\* | 7.\*
- [illuminate/support](https://github.com/illuminate/support): 5.\* | 6.\* | 7.\* | 8.\*

## Requirements

You will need to create a free account with [CometChat](https://cometchat.com), create an app and create a rest api key.
You will need to define your app id, api key and the api url in the config file. The api url can be obtained from the
reference [docs](https://prodocs.cometchat.com/reference), ie. (https://api-us.cometchat.io/v2.0)

## Install

You can install the package via [Composer](https://getcomposer.org/)

```bash
$ composer require rkcreative/laravel-cometchat
```

In Laravel 5.5 or above the service provider will automatically get registered. In older versions of the framework just
add the service provider in `config/app.php` file:

```php
'providers' => [
    ...
    /*
    * Package Service Providers...
    */
    
    RKCreative\LaravelCometChat\LaravelCometChatServiceProvider::class,   
    ...
],

'aliases' => [
    ...
    'CometChat'      => RKCreative\LaravelCometChat\LaravelCometChatFacade::class,
    ...
],
```

You can publish the config file with:

```bash
$ php artisan vendor:publish --provider="RKCreative\LaravelCometChat\LaravelCometChatServiceProvider" --tag=config
```

When published, [the `config/cometchat.php` config](config/cometchat.php) file contains:

```php
<?php

return [
    /*
    |--------------------------------------------------------------------------
    | CometChat API Key
    |--------------------------------------------------------------------------
    |
    | You can generate a rest api key for your app from the CometChat dashboard
    | and enter it here
    |
    */
    'api_key' => env('COMETCHAT_API_KEY', ''),

    /*
    |--------------------------------------------------------------------------
    | CometChat API Url
    |--------------------------------------------------------------------------
    |
    | Add the CometChat API url here
    |
    */
    'api_url'   => env('COMETCHAT_API_URL', 'https://api-us.cometchat.io/v2.0'),

    /*
    |--------------------------------------------------------------------------
    | App ID
    |--------------------------------------------------------------------------
    |
    | The App ID that was generated when you created your app from the dashboard.
    |
    */
    'app_id'    => config('COMETCHAT_APP_ID', ''),
];
```

## Usage

Each action below has required and optional parameters needed to return results. **You can read more
about [CometChat API Options](https://prodocs.cometchat.com/reference).**

### Facade

---

#### Create a User

```php
CometChat::createUser($uid, $name, $parameters = [])
```

Accepts:

- uid `(string):required` A unique identifier for the user.
- name `(string):required` A label or name such as a username for the user.
- parameters `(array):optional` An optional array of parameters provided
  by [CometChat](https://prodocs.cometchat.com/reference#createuser).

Returns on _success_:

- data `(object)` A data object
    - uid `(string);` A unique user id.
    - name `(string)` A name or label for the user.
    - status `(string)` The online/offline status of the user.
    - createdAt `(datetime)` The date of creation of the user.
    - authToken `(string)` When `withAuthToken` is included in the request and set to `true`, an `authToken` for the
      user is returned.
    - Any additional parameters that were passed in optionally.

Example:

```php
$user = \CometChat::createUser('12345', 'User1', ['withAuthToken' => true]);
$uid = $user->uid;
$name = $user->name;
$status = $user->status;
$createdAt = $user->createdAt;
$authToken = $user->authToken;
```

---

#### Update a User

```php
CometChat::updateUser($uid, $parameters = [])
```

Accepts:

- uid `(string):required` The uid when the user was added.
- parameters `(array):optional` An optional array of parameters provided
  by [CometChat](https://prodocs.cometchat.com/reference#updateuser).

Returns on _success_:

- data `(object)` A data object
    - uid `(string);` A unique user id.
    - name `(string)` A name or label for the user.
    - status `(string)` The online/offline status of the user.
    - createdAt `(datetime)` The date of creation of the user.
    - updatedAt `(datetime)` The date of update of the user.
    - Any additional parameters that were passed in optionally.

Example:

```php
$user = \CometChat::updateUser('12345', ['name' => 'NewUsername']);
$uid = $user->uid;
$name = $user->name;
$status = $user->status;
$createdAt = $user->createdAt;
$updatedAt = $user->updatedAt;
```

---

#### Delete a User

```php
CometChat::deleteUser($uid)
```

Accepts:

- uid `(string):required` The uid when the user was added.

Returns on _success_:

- data `(object)` A data object
    - message `(string);` A success message noting the user who was deleted.

Example:

```php
\CometChat::deleteUser('12345');
```

---

#### Deactivate a User

```php
CometChat::deactivateUser($uid)
```

Accepts:

- uid `(string):required` The uid when the user was added.

Returns on _success_:

- data `(object)` A data object
    - message `(string);` A success message noting the user who was deactivated.

Example:

```php
\CometChat::deactivateUser('12345');
```

---

#### Reactivate a User or Users

```php
CometChat::reactivateUser($uids)
```

Accepts:

- uids `(array):required` An array of user ids to reactivate.

Returns on _success_:

- data `(object)` A data object containing user objects of requested reactivations. Example:

```php
\CometChat::reactivateUser('12345');
```

---

#### Create a User Auth Token

```php
CometChat::createToken($uid, $force === false)
```

Accepts:

- uid `(string):required` The uid when the user was added.
- force `(boolean):optional` Generates new auth token forcefully.

Returns on _success_:

- data `(object)` A data object
    - uid `(string);` The UID of the user.
    - authToken `(string)` The authToken for the user.
    - createdAt `(datetime)` The date of creation of the authToken.

Example:

```php
$result = \CometChat::createToken('12345');
$authToken = $result->authToken;
```

---

#### Delete a User Token

```php
CometChat::deleteToken($uid, $authToken)
```

Accepts:

- uid `(string):required` The UID when the user was added.
- authToken `(string):required` The authToken for the user that is to be deleted.

Returns on _success_:

- data `(object)` A data object
    - success `(boolean);` A success status if true/false.
    - message `(string);` A status message of the request.

Example:

```php
\CometChat::deleteToken('12345', '3lk3kdk333ksliu');
```

---

#### Block Users

```php
CometChat::blockUsers($uid, $blockedUsers = [])
```

Accepts:

- uid `(string):required` The UID of the user who is adding the blocked users.
- blockedUsers `(array):required` An array of UID's to be blocked by the user.

Returns on _success_:

- A data object with UID objects that were blocked successfully.

Example:

```php
\CometChat::blockUsers('12345', ['12346', '12347', '12348']);
```

---

#### UnBlock Users

```php
CometChat::unblockUsers($uid, $blockedUsers = [])
```

Accepts:

- uid `(string):required` The UID of the user who is removing the blocked users.
- blockedUsers `(array):required` An array of UID's to be unblocked by the user.

Returns on _success_:

- A data object with UID objects that were unblocked successfully.

Example:

```php
\CometChat::unblockUsers('12345', ['12346', '12347', '12348']);
```

---

#### Add Friends

```php
CometChat::addFriends($uid, $friends = [])
```

Accepts:

- uid `(string):required` The UID of the user who is adding the friends.
- friends `(array):required` An array of UID's to be added as friends by the user.

Returns on _success_:

- A data object with UID objects that were added successfully.

Example:

```php
\CometChat::addFriends('12345', ['12346', '12347', '12348']);
```

---

#### Remove Friends

```php
CometChat::removeFriends($uid, $friends = [])
```

Accepts:

- uid `(string):required` The UID of the user who is removing the friends.
- friends `(array):required` An array of UID's to be removed by the user.

Returns on _success_:

- A data object with UID objects that were unblocked successfully.

Example:

```php
\CometChat::removeFriends('12345', ['12346', '12347', '12348']);
```

---

#### Reserved for future actions

---

## Testing

``` bash
$ composer test
```

## Contributing

### Adding code

The current set of actions that this library supports is incomplete. Additional actions will be added as time permits.
If you wish to add to this library, please create a pull request.

### Security Vulnerabilities

If you discover any security-related issues, please use the issue tracker. All security vulnerabilities will be promptly
addressed.

## License

The Laravel CometChat package is open-source software licensed under the [MIT license](LICENSE.md).
